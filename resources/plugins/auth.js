export default function({ app, env }) {
  const redirect = function(name, noRouter = false) {
    const callback = `${env.appUrl}/guest`;
    const to = `${env.authHost}/redirect?to=/signin?redirect=${callback}`;
    
    if (name === "login") {
      if (noRouter) {
        window.location.replace(to);
      } else {
        this.ctx.redirect(to);
      }
    }
  };

  app.$auth.redirect = redirect.bind(app.$auth);
}
