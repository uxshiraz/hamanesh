import Vue from "vue";

import Logo from "@/components/Logo";
import Spinner from "@/components/Spinner";
import Timer from "@/components/Timer";

Vue.component("Logo", Logo);
Vue.component("Spinner", Spinner);
Vue.component("Timer", Timer);
