const moment = require("moment-jalaali");
moment.loadPersian({ usePersianDigits: false });

export default (ctx, inject) => {
  inject("moment", moment);
};
