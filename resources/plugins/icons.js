/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from "vue";

import { BIconCheck, BIconCloudArrowUp, BIconPlus, BIconTrash, BIconX } from "bootstrap-vue";

Vue.component("BIconCheck", BIconCheck);
Vue.component("BIconCloudArrowUp", BIconCloudArrowUp);
Vue.component("BIconPlus", BIconPlus);
Vue.component("BIconTrash", BIconTrash);
Vue.component("BIconX", BIconX);
