export default ({ app }, inject) => {
  inject("acl", {
    get isAdmin() {
      if (app.$auth.loggedIn && process.env.adminUsers) {
        return process.env.adminUsers.split(",").includes(app.$auth.user._id.toString());
      }

      return false;
    }
  });
};
