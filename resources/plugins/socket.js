import WebSocket from "@adonisjs/websocket-client";

export default ({ $auth, env }, inject) => {
  const ws = WebSocket(null, { path: "ws" });

  ws.getChannel = channel =>
    ws.getSubscription(channel) || ws.subscribe(channel);

  // const token = $auth.getToken($auth.strategy.name).replace("Bearer ", "");
  // ws.withJwtToken(token);

  ws.connect();

  inject("socket", ws);
};
