import Vue from "vue";

Vue.filter("fn", value => {
  return value.toString().replace(/0-9/g, d => "٠١٢٣٤٥٦٧٨٩".indexOf(d));
});

Vue.filter("qoption", value => {
  switch (value.toString().toLowerCase()) {
    case "a":
      return "الف";
    case "b":
      return "ب";
    case "c":
      return "ج";
    case "d":
      return "د";
  }
});
