export const state = () => ({
  view: "Warmup"
});

export const mutations = {
  setView(state, view) {
    state.view = view;
  }
};

export const actions = {
  nextQuestion({ commit }) {
    commit("setView", "Question");
  },
  showLeaderboard({ commit }) {
    commit("setView", "Leaderboard");
  }
};
