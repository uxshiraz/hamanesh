export default function({ app, redirect }) {
  if (!app.$acl.isAdmin) {
    return redirect("/");
  }
}
