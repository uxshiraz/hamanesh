const { resolve } = require("path");

module.exports = {
  dev: process.env.NODE_ENV === "development",
  srcDir: resolve(__dirname, "..", "resources"),
  telemetry: false,

  ssr: false,

  env: {
    appUrl: process.env.APP_URL,
    appKey: process.env.APP_KEY,
    authHost: process.env.AUTH_HOST,
    authCookieName: "uxs.auth._token.local",
    sessionId: process.env.SESSION_ID,
    adminUsers: process.env.ADMIN_USERS
  },

  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: "fa",
      dir: "rtl"
    },
    title: "همانش",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "msapplication-TileColor",
        name: "msapplication-TileColor",
        content: "#ffffff"
      },
      {
        hid: "msapplication-config",
        name: "msapplication-config",
        content: "/icons/browserconfig.xml?v=m2nOm7oR7w"
      },
      { hid: "theme-color", name: "theme-color", content: "#ffb300" },
      { hid: "og:locale", name: "og:locale", content: "fa_IR" },
      {
        hid: "og:site_name",
        name: "og:site_name",
        content: "همانش"
      }
    ],
    link: [
      {
        rel: "apple-touch-icon",
        href: "/icons/apple-touch-icon.png?v=m2nOm7oR7w",
        sizes: "180x180"
      },
      {
        rel: "icon",
        href: "/icons/favicon-32x32.png?v=m2nOm7oR7w",
        type: "image/png",
        sizes: "32x32"
      },
      {
        rel: "icon",
        href: "/icons/favicon-16x16.png?v=m2nOm7oR7w",
        type: "image/png",
        sizes: "16x16"
      },
      { rel: "manifest", href: "/icons/site.webmanifest?v=m2nOm7oR7w" },
      {
        rel: "mask-icon",
        href: "/icons/safari-pinned-tab.svg?v=m2nOm7oR7w",
        color: "#f05730"
      },
      {
        rel: "shortcut icon",
        href: "/icons/favicon.ico?v=m2nOm7oR7w"
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#ffb300" },
  /*
   ** Global CSS
   */
  css: ["@/assets/scss/index.scss"],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "@/plugins/bootstrap-ui.js",
    "@/plugins/icons.js",
    "@/plugins/i18n.js",
    "@/plugins/components.js",
    "@/plugins/filters.js",
    "@/plugins/moment.js",
    "@/plugins/acl.js",
    "@/plugins/aos"
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    "@nuxtjs/eslint-module"
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    // Doc: https://github.com/nuxt-community/dotenv-module
    "@nuxtjs/dotenv",
    "@nuxtjs/auth",
    "@nuxtjs/style-resources",
    "cookie-universal-nuxt"
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.API_URL
  },

  router: {
    middleware: ["auth"]
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `${process.env.APP_URL}/auth`,
            method: "post",
            propertyName: "data.token"
          },
          user: {
            url: `${process.env.APP_URL}/auth/me`,
            method: "get",
            propertyName: "data"
          },
          logout: false
        }
      }
    },

    plugins: ["@/plugins/socket.js", "@/plugins/auth.js"],

    cookie: {
      prefix: "hm.auth.",
      options: {
        sameSite: "strict",
        domain: process.env.AUTH_ORIGIN
      }
    }
  },

  styleResources: {
    scss: ["@/assets/scss/vars.scss"]
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extractCSS: process.env.NODE_ENV === "production",

    extend(config, ctx) {}
  }
};
