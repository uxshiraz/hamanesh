"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use("Route");

Route.group(() => {
  Route.post("/", "AuthController.authentication");
  Route.get("/me", "AuthController.me").middleware("auth");
}).prefix("/auth");

Route.group(() => {
  Route.post("/sessions", "SessionController.create");
  Route.get("/sessions/default", "SessionController.showDefault");
  Route.get("/sessions/:id", "SessionController.show");
  Route.post("/sessions/:id/questions", "SessionController.updateQuestions");
  Route.put("/sessions/:id/questions/:qid", "SessionController.setQuestion");
  Route.post("/sessions/:id/questions/:qid", "SessionController.answerQuestions").middleware('auth');
  Route.delete("/sessions/:id/questions/:qid", "SessionController.deleteQuestions");
  Route.put("/sessions/:id/timeout", "SessionController.timeoutQuestion");
  Route.get("/sessions/:id/answers", "SessionController.listAnswers").middleware('auth');
  Route.get("/sessions/:id/leaderboard", "SessionController.leaderboard");
}).prefix("/api");

Route.any("*", "NuxtController.render");
