"use strict";

const got = use("got");
const User = use("App/Models/User");

class AuthController {
  /**
   * authenticate the user with third party website and
   * return the token to the front end
   * @param {request}
   * @param {response}
   * @param {auth}
   */
  async authentication({ request, response, auth }) {
    const { token } = request.all();

    await got
      .get(`${process.env.AUTH_HOST}/auth/me`, {
        headers: { Authorization: token },
        responseType: "json"
      })
      .then(async res => {
        const userInfo = res.body.data;

        const data = {
          name: userInfo.name,
          email: userInfo.email,
          avatar: userInfo.avatar
        };

        let user = await User.where({ email: userInfo.email }).first();

        if (!user) {
          user = new User();
          user.fill(data);
        } else {
          user.merge(data);
        }

        await user.save();

        const token = await auth.generate(user);

        return response.json({
          code: "200",
          data: token
        });
      })
      .catch(error => {
        if (error.response) {
          return response.status(401).json({
            error: error.response.statusCode,
            message: error
          });
        }

        return response.status(500).json({
          error: 500,
          message: error
        });
      });
  }

  me({ auth }) {
    return {
      data: auth.user.toJSON()
    };
  }
}

module.exports = AuthController;
