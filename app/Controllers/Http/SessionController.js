"use strict";

const Ws = use("Ws");
const omit = use("lodash/omit");
const Session = use("App/Models/Session");
const SessionsAnswer = use("App/Models/SessionsAnswer");
const Crypto = use("crypto-js");

class SessionController {
  async show({ params, transform }) {
    const session = await Session.find(params.id);

    return transform.item(session, "SessionTransformer");
    // return session.toJSON();
  }

  async showDefault() {
    const session = await Session.first();

    if (session) return session.toJSON();

    return null;
  }

  async create() {
    const session = await Session.create({
      questions: {},
      current: null,
      countdown: 20,
      successScore: 10,
      failureScore: 1
    });

    return session.toJSON();
  }

  async listAnswers({ params, auth }) {
    const session = await Session.find(params.id);

    const userAnswers = await SessionsAnswer.where("user_id", auth.user._id)
      .where("session_id", session._id)
      .first();

    if (userAnswers) return userAnswers.toJSON();
    else return { answers: {} };
  }

  async updateQuestions({ request, params }) {
    const session = await Session.find(params.id);

    const { questions } = request.only("questions");

    session.merge({ questions });
    await session.save();
  }

  async setQuestion({ params }) {
    try {
      const session = await Session.find(params.id);

      const current = session.questions[params.qid];
      current.timeout = false;

      session.merge({ current });
      await session.save();

      current.answer = Crypto.AES.encrypt(
        current.answer,
        process.env.APP_KEY
      ).toString();

      const topic = Ws.getChannel("session:*").topic(`session:${session._id}`);

      if (topic) {
        topic.broadcast("newQuestion", current);
      }
    } catch (e) {}
  }

  async timeoutQuestion({ params }) {
    const session = await Session.find(params.id);

    if (session.current) {
      session.merge({
        current: {
          ...session.current,
          timeout: true
        }
      });

      await session.save();
    }
  }

  async deleteQuestions({ params }) {
    const session = await Session.find(params.id);
    const questions = omit(session.questions, params.qid);

    session.merge({ questions });
    await session.save();
  }

  async answerQuestions({ params, request, auth, response }) {
    const session = await Session.find(params.id);
    const question = session.current;

    if (question.timeout) {
      return response.status(422).json({
        error: 422,
        message: "Question timeout"
      });
    }

    const userAnswers = await SessionsAnswer.where("user_id", auth.user._id)
      .where("session_id", session._id)
      .first();

    const { answer } = request.only(["answer"]);

    const succeed = answer === question.answer;
    const point = succeed ? session.successScore : session.failureScore;

    if (!userAnswers) {
      await SessionsAnswer.create({
        user_id: auth.user._id,
        session_id: session._id,
        answers: {
          [question._id]: {
            answer,
            succeed,
            point
          }
        }
      });
    } else {
      const answers = userAnswers.answers;

      answers[question._id] = {
        answer,
        succeed,
        point
      };

      userAnswers.merge({
        answers
      });

      await userAnswers.save();
    }
  }

  async leaderboard({ params }) {
    const session = await Session.find(params.id);

    const answers = await SessionsAnswer.with(["user"])
      .where("session_id", session._id)
      .fetch();

    return answers.toJSON().map(item => ({
      name: item.user.name,
      avatar: item.user.avatar,
      ...Object.keys(item.answers).reduce(
        (obj, key) => {
          const q = item.answers[key];
          return {
            ...obj,
            score: obj.score + q.point,
            succeed: obj.succeed + (q.succeed ? 1 : 0)
          };
        },
        { score: 0, succeed: 0 }
      )
    }));
  }
}

module.exports = SessionController;
