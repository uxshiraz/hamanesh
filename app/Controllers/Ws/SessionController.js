"use strict";

const subscribers = {};
let adminSocket = null;

function incrementUser(socket) {
  if (!subscribers[socket.topic]) {
    subscribers[socket.topic] = [];
  }

  subscribers[socket.topic].push(socket.id);
}

function decrementUser(socket) {
  if (!subscribers[socket.topic]) {
    subscribers[socket.topic] = [];
  } else {
    const i = subscribers[socket.topic].indexOf(socket.id);
    if (i >= 0) {
      subscribers[socket.topic].splice(i, 1);
    }
  }
}

function liveUsers(socket) {
  if (!subscribers[socket.topic]) return 0;
  
  return subscribers[socket.topic].length;
}

class SessionController {
  constructor({ socket, request, auth }) {
    this.socket = socket;
    this.topic = socket.topic;
    this.request = request;
    this.auth = auth;
  }

  onHello() {
    incrementUser(this.socket);

    if (adminSocket) {
      adminSocket.emit("liveUsers", liveUsers(this.socket));
    }
  }

  onAdminHello() {
    adminSocket = this.socket;
    this.socket.emit("liveUsers", liveUsers(this.socket));
  }

  onGetAlives() {
    adminSocket.emit("liveUsers", liveUsers(this.socket));
  }

  onClose() {
    decrementUser(this.socket);

    if (adminSocket) {
      adminSocket.emit("liveUsers", liveUsers(this.socket));
    }
  }
}

module.exports = SessionController;
