"use strict";

const Session = use("App/Models/Session");
const SessionsAnswer = use("App/Models/SessionsAnswer");

class PerformerController {
  constructor({ socket, request }) {
    this.socket = socket;
    this.request = request;
  }

  async onGetAnswers({ id, qid }) {
    const session = await Session.find(id);

    const answers = await SessionsAnswer.where(
      "session_id",
      session._id
    ).fetch();

    if (answers) {
      const total = answers.toJSON().reduce((count, item) => {
        if (item.answers && item.answers[qid]) {
          return count + 1;
        }

        return count;
      }, 0);

      this.socket.emit("answersCount", total);
    } else {
      this.socket.emit("answersCount", 0);
    }
  }
}

module.exports = PerformerController;
