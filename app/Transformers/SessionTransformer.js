"use strict";

const { map } = use("lodash");
const Crypto = use("crypto-js");
const BumblebeeTransformer = use("Bumblebee/Transformer");

/**
 * SessionTransformer class
 *
 * @class SessionTransformer
 * @constructor
 */
class SessionTransformer extends BumblebeeTransformer {
  /**
   * This method is used to transform the data.
   */
  transform(model) {
    const session = model.toJSON();

    const questions = {};

    map(session.questions, item => {
      questions[item._id] = {
        ...item,
        answer: Crypto.AES.encrypt(item.answer, process.env.APP_KEY).toString()
      };
    });

    return {
      ...session,
      current: session.current
        ? {
            ...session.current,
            answer: Crypto.AES.encrypt(
              session.current.answer,
              process.env.APP_KEY
            ).toString()
          }
        : null,
      questions
    };
  }
}

module.exports = SessionTransformer;
